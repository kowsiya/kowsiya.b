package bank.acc.sys;

import java.util.Scanner;

public class BankSystem {

	Scanner scan = new Scanner(System.in);
	double currentBalance = 0;
	int numberOfDeposit = 0;
	int numberOfWithdrawals = 0;
	double amount = 0;

	public void Balance() {
		System.out.println("Enter your current balance : ");
		currentBalance = scan.nextDouble();
	}

	public void Deposit() {
		do {
			System.out.println("Enter the number of deposits (0 - 5) : ");
			numberOfDeposit = scan.nextInt();

		} while (numberOfDeposit > 5 || numberOfDeposit < 0);
	}

	public void Withdrawals() {
		do {
			System.out.println("Enter the number of withdrawals (0 - 5) : ");
			numberOfWithdrawals = scan.nextInt();
		} while (numberOfWithdrawals > 5 || numberOfWithdrawals < 0);
	}

	public void deposit() {

		for (int i = 1; i <= numberOfDeposit; i++) {
			System.out.println("Enter the amount of deposit #" + i + " :");
			double amount = scan.nextDouble();

			if (amount <= 0) {
				System.out.println("***Deposit amount must be greater than zero, please re-enter. ");
				amount = scan.nextDouble();
			}
			currentBalance += amount;
		}
	}

	public void withdrawal() {

		for (int i = 1; i <= numberOfWithdrawals; i++) {
			do {
				System.out.println("Enter the amount of Withdrawal #" + i + " :");
				amount = scan.nextDouble();

				if (amount <= 0) {
					System.out.println("***Withdrawl amount must be greater than zero, please re-enter. ");
					amount = scan.nextDouble();
				}

				if (amount > currentBalance) {
					System.out.println("***Withdrawal amount exceeds current  balance. ");

				}
			} while (amount > currentBalance);

			currentBalance -= amount;
		}
		System.out.println("closing balance is : " + currentBalance);
		if (currentBalance >= 50000.00) {
			System.out.println("*** it is time to invest some mone]y.***");
		}
		if (currentBalance >= 15000.00) {
			System.out.println("*** you should consider a CD.***");
		} else if (currentBalance >= 1000.00) {
			System.out.println("*** keep up the good work.***");
		} else {
			System.out.println("*** your balance is getting low.***");
		}

	}
}
